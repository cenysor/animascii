#! /usr/bin/python3

# A program that displays animated ASCII art

import os
import time
import sys


print('Animascii lets you display animated ASCII art')

while True:
    try:
        print('Please enter [path/file], [demo], [help] or [exit]')
        path = input()

        if path == 'help':
            print('A textfile must contain in its first line the number of lines to display at once')
            print('All following lines must have the same amount of characters, blank spaces included')
            print('See demo.txt and demo2.txt for example')
            print('Please enter [path/file], [demo] or [exit]')
            path = input()

        if path == 'exit':
            sys.exit()

        if path == 'demo':
            textfile = open('demo.txt')
            break

        else:
            textfile = open(path)
            break
    except FileNotFoundError:
        print('Invalid file path')
        continue

textlist = textfile.readlines()

print('\033[2J')
while True:
    for i in range(1, len(textlist)-int(textlist[0])+1, int(textlist[0])):
        s = ""
        s += '\033[H'
        for a in range(0, int(textlist[0])):
            s += "%s" % (textlist[i+a])
        print(s)
        print('Press Ctrl+C to exit')
        time.sleep(.1)